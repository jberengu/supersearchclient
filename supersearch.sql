drop database if exists supersearch;
create database supersearch;
\c supersearch

drop role if exists searcher;
create user searcher with password 'g~N]mD:>(9y2A,5S';

--
-- data for table movies
drop table if exists movies;
drop table if exists storetypes;
drop table if exists stores;
drop table if exists users;

create table users (
	userid serial PRIMARY KEY,
	username text,
	password text,
	zip int
);

create table movies (
	movieid serial PRIMARY KEY,
	movie text,
	theater text,
	address text,
	city text,
	zip int
);

create table stores (
	storeid serial PRIMARY KEY,
	name text,
	address text,
	city text,
	zip int
);

create table storetypes (
	typeid serial PRIMARY KEY,
	storeid int,
	type text,
	FOREIGN KEY (storeid) REFERENCES stores (storeid)
);

INSERT INTO movies (movie, theater, address, city, zip) VALUES
('Nobody''s Watching', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('It', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('The Limehouse Golem', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('Despicable Me 3', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('Wonder Woman', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('The Emoji Movie', 'Regal Fredericksburg 15', '3301 Plank Road Route 3W', 'Fredericksburg', 22401),
('Year By The Sea', 'Marquee Cinemas Southpoint 9', '5800 South Point Centre', 'Fredericksburg',  22401),
('Rememory', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005),
('Wonder Woman', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005),
('Dunkirk', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005),
('Anti Matter', 'Allen Cinema 4 Mesilla Valley', '700 South Telshor Boulevard', 'Las Cruces', 88005);

-- --------------------------------------------------------

--
-- Data for table stores
--

INSERT INTO stores (name, address, city, zip) VALUES
('Hyperion Espresso', '301 William St.',  'Fredericksburg', 22401),
('Starbucks', '2001 Plank Road', 'Fredericksburg', 22401),
('25 30 Expresso', '400 Princess Anne St', 'Fredericksburg', 22401),
('Agora Downtown', '520 Caroline St', 'Fredericksburg', 22401),
('Highpoint Coffee', '615 Caroline St', 'Fredericksburg', 22401),
('Duck Donuts', '1223 Jefferson Davis Hwy', 'Fredericksburg', 22401),
('Basilico', '2577 Cowan Blvd', 'Fredericksburg',  22401),
('Cork and Table', '909 Caroline', 'Fredericksburg',  22401),
('Orofino', '1251 Carl D Silver Parkway', 'Fredericksburg',  22401),
('Pancho Villa Mexican Rest', '10500 Spotsylvania Ave', 'Fredericksburg', 22401),
('Chipotle', '5955 Kingstowne', 'Fredericksburg', 22401),
('Sedona Taphouse', '591 Williams', 'Fredericksburg', 22401),
('Pueblo''s Tex Mex Grill', '1320 Jefferson Davis Hwy', 'Fredericksburg', 22401),
('El Pino', '4211 Plank Road', 'Fredericksburg', 22401),
('Starbucks', '2808 Telshor Blvd', 'Las Cruces', 88005),
('Starbucks', '2511 Lohman Ave', 'Las Cruces', 88005),
('Milagro Coffee Y Espresso', '1733 E. University Ave', 'Las Cruces', 88005),
('Starbucks', '1500 South Valley',  'Las Cruces', 88005),
('Bean', '2011 Avenida De Mesilla',  'Las Cruces', 88005),
('El Comedor', '2190 Avenida De  Mesilla', 'Las Cruces', 88005),
('Los Compas', '603 S Nevarez St.',  'Las Cruces', 88005),
('La Fuente', '1710 S Espina',  'Las Cruces', 88005),
('La Posta', '2410 Calle De San Albino',  'Las Cruces', 88005),
('El Jacalito', '2215 Missouri Ave',  'Las Cruces', 88005),
('Peet''s', '2260 Locust',  'Las Cruces', 88005);

-- --------------------------------------------------------

--
-- Data for storetypes
--
INSERT INTO storetypes (storeid, type) VALUES 
(1, 'coffee'),
(1, 'donuts'),
(2, 'coffee'),
(3, 'coffee'),
(3, 'American'),
(4, 'coffee'),
(4, 'Italian'),
(5, 'coffee'),
(5, 'Mexican restaurant'),
(6, 'coffee'),
(6, 'donuts'),
(6, 'American'),
(7, 'Italian'),
(8, 'American'),
(8, 'Italian'),
(9, 'Italian'),
(10, 'Mexican restaurant'),
(11, 'Mexican restaurant'),
(12, 'American'),
(12, 'Mexican restaurant'),
(13, 'Mexican restaurant'),
(14, 'Mexican restaurant'),
(15, 'coffee'),
(16, 'coffee'),
(16, 'donuts'),
(17, 'coffee'),
(18, 'coffee'),
(19, 'coffee'),
(20, 'Mexican restaurant'),
(20, 'Italian'),
(21, 'Mexican restaurant'),
(22, 'Mexican restaurant'),
(23, 'Mexican restaurant'),
(23, 'American'),
(24, 'Mexican restaurant'),
(25, 'coffee');

INSERT INTO users (username, password, zip) VALUES 
('jberengu', '$2a$10$5XzkL72nCJFPLwPeaHnapeBNdhCh2oGLBni6BsOyrxun2nxdQX6hO', 22401),
('mhendo', '$2a$10$/YguM/.QqoMEJLi8PuN/G.Lr8PbJ5IOc8r45D1mxqYtlvyKZBnvNe', 22401),
('imirg', '$2a$10$j7KI7581wy7lUpXH0l3XyeFHOUeS4j6o9MOaFWnAJVQYpFRR5gLsy', 88005),
('smirg', '$2a$10$KCHF2Oenu16TXFMvDyNi5ukd/yJylVlGsBmdr9OUFdm8vO27DwZTa', 88005);

grant all on movies, stores, storetypes,users to searcher;
grant usage, select on all sequences in schema public to searcher;


