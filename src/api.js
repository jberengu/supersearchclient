var axios = require('axios');

function allmovies(id){
		var encodedURI = ('http://localhost:3001/allmovies?id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'movies'};
		});
}

function movies(name, id){
		var encodedURI = ('http://localhost:3001/movies?entry='+name+'&id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'movies'};
		});
}

function type(type, id){
		var encodedURI = ('http://localhost:3001/type?entry='+type+'&id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'type'};
		});
}

function store(name, id){
		var encodedURI = ('http://localhost:3001/store?entry='+name+'&id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'store'};
		});
}

module.exports= {
	info: function(term,id){
		if(term=== 'movies'){
			return allmovies(id);
		}else{
			var data= movies(term,id);
			var data2= store(term,id);
			var data3= type(term,id);
			if(data!== {entries: []}){
				return movies(term,id);
			}
			else if(data2.info!== {entries: []}){
				return store(term,id);
			}
			else if(data3.info!== {entries: []}){
				return type(term,id);
			}
		}
	},
	login: function (username, password){
		var encodedURI = ('http://localhost:3001/user?username='+username+'&password='+password);
		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return response.data;
		});
	},
	getlogin: function (username){
		var encodedURI = ('http://localhost:3001/userid?username='+username);
		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return response.data;
		});
	},
	store: function (name, id){
		var encodedURI = ('http://localhost:3001/store?entry='+name+'&id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'store'};
		});
	},
	type: function (type, id){
		var encodedURI = ('http://localhost:3001/type?entry='+type+'&id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'type'};
		});
	},
	movies: function (name, id){
		var encodedURI = ('http://localhost:3001/movies?entry='+name+'&id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'movies'};
		});
	},
	allmovies: function (id){
		var encodedURI = ('http://localhost:3001/allmovies?id='+id);

		return axios.get(encodedURI).then(function (response) {
			console.log(response.data);
			return {info:response.data, status: 'movies'};
		});
	},
	createuser: function (username, password, zip){
		var encodedURI = ('http://localhost:3001/create-user?username='+username+'&password='+password+'&zip='+zip);

		return axios.post(encodedURI).then(function (response) {
			console.log("response: "+response.data);
			return response.data;
		});
	}
}