var React = require('react');
var api= require('./api');
var data;
var stat;


  class Search extends React.Component {
     constructor(props) {
      super(props);
      this.state = {
        searchTerm: '',
        data: { info: {entries:[]}, status: ''},
        status: '',
        userid: ''
      };
      this.updateSearch= this.updateSearch.bind(this);
    } 

    updateSearch(event) {
        this.setState({ searchTerm: event.target.value.substr(0, 20) });
        if(event.target.value===''){
          this.setState(function(){
                    return{
                    data: { info: {entries:[]}, status: ''},
                    }
                  });
        }
        this.setState(function(){
                    return{
                      status: ''
                    }
        });
        stat='';
        console.log(event.target.value);
        console.log(this.state.status);
        if(!this.props.username){
          if(event.target.value==='movies'){
            console.log('in');
            api.allmovies('').then((results)=>{
              stat='something';
              this.setState(function(){
                  return{
                  data: results,
                  status: results.status
                  }
                })
            });
          }else{
            api.type(event.target.value,'').then((results)=>{
              console.log(results.info.entries.length);
              if(results.info.entries.length>0){
                stat='something';
                this.setState(function(){
                  return{
                  data: results,
                  status: results.status
                  }
                })
              }
            });
            console.log(stat +' stat value');
            if(stat===''){
              api.store(event.target.value,'').then((results)=>{
                console.log(results.info.entries.length);
                if(results.info.entries.length>0){
                  stat='something';
                  this.setState(function(){
                    return{
                      data: results,
                      status: results.status
                    }
                  })
                } 
              });
            
            if(stat===''){
                api.movies(event.target.value,'').then((results)=>{
                console.log(results.info.entries.length);
                if(results.info.entries.length>0){
                  stat='something';
                  this.setState(function(){
                    return{
                      data: results,
                      status: results.status,
                    }
                  })
                }
              });
            if(stat===''){
                this.setState(function(){
                    return{
                      data: { info: {entries:[]}, status: ''},
                    }
                  })
            }}
            }
          }
        }else{
          console.log('logged in');
          api.getlogin(this.props.username).then((results) => {
            return this.setState(()=> ({
              userid: results.status.userid
            }));
          });
          if(event.target.value==='movies'){
            console.log('in');
            api.allmovies(this.state.userid).then((results)=>{
              this.setState(function(){
                  return{
                  data: results,
                  status: results.status
                  }
                })
            });
          }else{
            api.movies(event.target.value,this.state.userid).then((results)=>{
              console.log(results.info.entries.length);
              if(results.info.entries.length>0){
                this.setState(function(){
                  return{
                  data: results,
                  status: results.status
                  }
                })
              }
            });
            if(this.state.status===''){
              api.store(event.target.value,this.state.userid).then((results)=>{
                console.log(results.info.entries.length);
                if(results.info.entries.length>0){
                  this.setState(function(){
                    return{
                      data: results,
                      status: results.status
                    }
                  })
                } 
              });
              if(this.state.status===''){
                api.type(event.target.value,this.state.userid).then((results)=>{
                console.log(results.info.entries.length);
                if(results.info.entries.length>0){
                  this.setState(function(){
                    return{
                      data: results,
                      status: results.status
                    }
                  })
                } 
              });
              }if(this.state.status===''){
                this.setState(function(){
                    return{
                      data: { info: {entries:[]}, status: ''},
                      status: ''
                    }
                  })
              }
            }
          }
        }
    }
  
    render() {
    const data = this.state.data;
    console.log(data);
     return (
      <div>
        <div >
          
          <p className="App-intro">
          </p>
          <p><input type="text" 
              className="rounded" 
              placeholder="Search Database..."
              onChange={this.updateSearch}
              value={this.state.searchTerm}  />
          </p>
          </div>
          <div>
        {data.status === 'movies' ? (
                <div>
                    <table className="table">
                        <tbody>
                           {data.info.entries.map(info => (<tr>
                                <td>{info.movie}</td>
                                <td>{info.theater}</td>
                                <td>{info.address}</td>
                                <td>{info.city}</td>
                                <td>{info.zip}</td>
                            </tr>))}
                        </tbody>
                    </table>
                </div>):(
          <div>
              <table className="table">
                  <tbody>
                     {data.info.entries.map(info => (<tr>
                          <td>{info.name}</td>
                          <td>{info.type}</td>
                          <td>{info.address}</td>
                          <td>{info.city}</td>
                          <td>{info.zip}</td>
                      </tr>))}
                  </tbody>
              </table>
          </div>)
        }
        </div></div>);
    }
  }
  
  module.exports  = Search;