var React = require('react');
var Link = require('react-router-dom').Link;
var api= require('./api');

class Signup extends React.Component {

   constructor(props){
      super(props);
      this.state = {
        username: '',
        password: '',
        zip: '',
        status: ''
      } 
      this.handleUsernameChange = this.handleUsernameChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleZipChange=this.handleZipChange.bind(this);
    }

    handleUsernameChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          username: value
        }
      })
    }

    handleZipChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          zip: value
        } 
      })
    }

    handlePasswordChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          password: value
        }
      })
    }

    handleSubmit(event){
      event.preventDefault();
      var that= this;
      api.createuser(that.state.username, that.state.password, that.state.zip).then((results) => {
      	if(results.status === 'success'){
          this.props.onSubmit(this.state.username, this.state.password, this.state.zip)
      	}else{
      		this.setState(function(){
      			return{
      				status: results.status
      			}
      		})
      	}
      });
    }



    render(){
        if (!this.props.username && !this.state.status){
        return (<div><h2>Create New User</h2>
        <form className='column' onSubmit={this.handleSubmit}>
        <p>
        
        <input
          id='username'
          className="rounded" 
          placeholder='username'
          type='text'
          
          autoComplete='off'
          value = {this.state.username}
          onChange={this.handleUsernameChange}
        /></p>

        <p>
        <input
          id='paswsword'
          className="rounded" 
          type='password'
          placeholder='password'
          autoComplete='off'
          value = {this.state.password}
          onChange={this.handlePasswordChange}
        /></p>

        <p>
        <input
          id='zip'
          className="rounded"
          placeholder='zip code'
          autoComplete='off'
          value = {this.state.zip}
          onChange={this.handleZipChange}
        /></p>


        <button
          className="button" 
          type='submit'
          disabled={!this.state.username || !this.state.password || !this.state.zip}
           >
            Submit
        </button>
      </form>
        
        </div>
    )
  }
  else {
  	if(this.state.status === 'failed'){
    	return (
    	  <h2>Failed Logged In</h2>)
  	}else{
  		return (<h2>Successfully Logged In</h2>)
  	}
  }
  }
}

module.exports = Signup;