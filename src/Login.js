var React = require('react');
var PropTypes = require('prop-types');
var api= require('./api');

class Login extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        username: '',
        password: '',
        status: ''
      } 
      this.handleChange = this.handleChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          username: value
        }
      })
    }

    handlePasswordChange(event){
      var value = event.target.value;
      this.setState(function(){
        return {
          password: value
        }
      })
    }

    handleSubmit(event){
      event.preventDefault();
      var that= this;
      api.login(that.state.username, that.state.password).then((results) => {
        console.log(results.entries);
        if (results.entries==='failed'){
          console.log(results.entries);
          this.setState(function(){
            return{
              status: results.entries
            }
          })
        }else{
          this.props.onSubmit(this.state.username, this.state.password, this.state.zip)
        }
      });
      console.log(this.state.status);
    }



    render(){
        if (!this.props.username && !this.state.status){
        return (<div><h2>login</h2>
        <form className='column' onSubmit={this.handleSubmit}>
        <p>
        
        <input
          id='username'
          className="rounded" 
          placeholder='username'
          type='text'
          
          autoComplete='off'
          value = {this.state.username}
          onChange={this.handleChange}
        /></p>

        <p>
        <input
          id='paswsword'
          className="rounded" 
          type='password'
          placeholder='password'
          autoComplete='off'
          value = {this.state.password}
          onChange={this.handlePasswordChange}
        /></p>


        <button
          className="button" 
          type='submit'
          disabled={!this.state.username || !this.state.password}
           >
            Submit
        </button>
      </form>
        
        </div>
    )
  }
  else {
      if(this.state.status === 'failed'){
        return (
          <h2>Failed Logged In</h2>)
      }else{
        return (<h2>Successfully Logged In</h2>)
    }
  }
  }

}

module.exports = Login;