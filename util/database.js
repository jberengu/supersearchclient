const express= require('express');
var Pool= require('pg').Pool;
var bodyParser= require('body-parser');
const bcrypt = require('bcrypt');

const app= express();
var config= {
	host: 'localhost',
	user: 'searcher',
	password: 'g~N]mD:>(9y2A,5S',
	database: 'supersearch',
};

var pool= new Pool(config);

app.set('port', (process.env.PORT || 3001));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

if (process.env.NODE_ENV === 'production'){
	console.log('running production server');
}
app.use(express.static('../build'));

app.get('/store', async (req, res) => {
	var entry= req.query.entry;
	var id= req.query.id;
	try{
		if(id == undefined || id == ""){
			var response= await pool.query('select s.name, st.type, s.address, s.city, s.zip from stores as s join storetypes as st on s.storeid = st.storeid where s.name like $1;', [entry+'%']);
			if (entry == undefined || entry == ""){
				res.json({entries: []})
			}else{
				res.json({entries: response.rows.map(function (item){
					return {name: item.name, type: item.type, address: item.address, city: item.city, zip: item.zip};
				})});
			}
		}else{
			var response= await pool.query('select s.name, st.type, s.address, s.city, s.zip from stores as s join storetypes as st on s.storeid = st.storeid where s.name like $1 and zip =(select zip from users where userid = $2);', [entry+'%',id]);
			if (entry == undefined || entry == ""){
				res.json({entries: []})
			}else{
				res.json({entries: response.rows.map(function (item){
					return {name: item.name, type: item.type, address: item.address, city: item.city, zip: item.zip};
				})});
			}
		}
		res.json({entries: []})
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/type', async (req, res) => {
	var entry= req.query.entry;
	var id= req.query.id;
	try{
		if(id == undefined || id == ""){
			var response= await pool.query('select s.name, st.type, s.address, s.city, s.zip from stores as s join storetypes as st on s.storeid = st.storeid where st.type like $1', [entry+'%']);
			if (entry == undefined || entry ==""){
				res.json({entries: []})
			}else{
				res.json({entries: response.rows.map(function (item){
					return {name: item.name, type: item.type, address: item.address, city: item.city, zip: item.zip};
				})});
			}
		}else {
			var response= await pool.query('select s.name, st.type, s.address, s.city, s.zip from stores as s join storetypes as st on s.storeid = st.storeid where st.type like $1 and zip =(select zip from users where userid = $2);', [entry+'%',id]);
			if (entry == undefined || entry == ""){
				res.json({entries: []})
			}else{
				res.json({entries: response.rows.map(function (item){
					return {name: item.name, type: item.type, address: item.address, city: item.city, zip: item.zip};
				})});
			}
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/movies', async (req, res) => {
	var entry= req.query.entry;
	var id= req.query.id;
	try{
		if(id == undefined || id == ""){
			var response= await pool.query('select movie, theater, address, city, zip from movies where movie like $1;', [entry+'%']);
			if (entry == undefined || entry == ""){
				res.json({entries: []})
			}else{
				res.json({entries: response.rows.map(function (item){
					return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip};
				})});
			}
		}else{
			var response= await pool.query('select movie, theater, address, city, zip from movies where movie like $1 and zip =(select zip from users where userid = $2);', [entry+'%',id]);
			if (entry == undefined || entry == ""){
				res.json({entries: []})
			}else{
				res.json({entries: response.rows.map(function (item){
					return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip};
				})});
			}
		}
		res.json({entries: []})
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/allmovies', async (req, res) => {
	var id= req.query.id;	
	try{
		if(id == undefined || id == ""){
			var response= await pool.query('select movie, theater, address, city, zip from movies;');
			res.json({entries: response.rows.map(function (item){
				return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip};
			})});
		}else{
			var response= await pool.query('select movie, theater, address, city, zip from movies where zip = (select zip from users where userid = $1);',[id]);
			res.json({entries: response.rows.map(function (item){
				return {movie: item.movie, theater: item.theater, address: item.address, city: item.city, zip: item.zip};
			})});
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/user', async (req, res) => {
	var username= req.query.username;
	var password= req.query.password;
	try{
		if((username == undefined || username == "") && (password == undefined || password == "")){
			res.json({status: 'failed'})
		}else{
			var check= await pool.query("select password from users where username= $1;", [username]);
			if(check.rowCount>0){
				if(bcrypt.compareSync(password, check.rows[0].password)) {
 					var response= await pool.query("select username, password, zip from users where username=$1;",[username]);
					res.json({entries: response.rows.map(function (item){
							return {username: item.username, password: item.password, zip: item.zip};
					})});
				} else {
 					res.json({entries: 'failed'});
				}
			}else{
				res.json({entries: 'failed'});
			}
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/userid', async (req, res)=> {
	var username= req.query.username;
	try{
		if(!username){
			req.json({status: 'failed'});
		}else{
			var response= await pool.query('select userid from users where username=$1',[username]);
			res.json({status: response.rows[0]});
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
})

app.post('/create-user', async (req, res) => {
	var username= req.query.username;
	var password= req.query.password;
	var zip= req.query.zip;
	let hash = bcrypt.hashSync(password, 10);

	if(!username || !password || !zip){
		req.json({status: 'failed'});
	}else{
		try {
			var exists= await pool.query('select userid from users where username= $1;',[username]);
			if (exists.rowCount < 1){
				var response= await pool.query('insert into users (username, password, zip) values ($1,$2,$3);',[username, hash, zip]);
				res.json({status: 'success'});
			}else{
				res.json({status: 'failed'});
			}
		}catch(e){
	    	console.error('Error running query '+ e);
	    	res.json({status: 'failed'});
	    }
	}
});

app.listen(app.get('port'), () => {
	console.log('Running');
})
